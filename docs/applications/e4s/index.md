# Extreme-scale Scientific Software Stack (E4S)

 The [Extreme-scale Scientific Software Stack (E4S)](https://e4s-project.github.io/) is
 a curated software stack from the [Spack](https://spack.readthedocs.io/en/latest/) ecosystem that 
 is continuously built and tested on several pre-exascale systems. E4S is composed of many
 open-source projects, including xSDK, dev-tools, math-libraries, compilers, and more. 
 For a complete product list see [here](https://e4s-project.github.io/Resources/ProductInfo.html). 

E4S is shipped as a container (Docker, Singularity, Shifter, CharlieCloud), a [buildcache](https://oaciss.uoregon.edu/e4s/inventory.html),
or a Spack manifest (`spack.yaml`). Currently, we focus on building E4S from source using 
a `spack.yaml` file provided by the E4S team from https://github.com/E4S-Project/e4s. 

!!! note 
    We install as many packages from E4S provided in spack.yaml as possible. Some 
    packages were intentionally skipped such as specs tied to `develop` branches 
    or packages built for GPUs. Some additional packages couldn't be installed
    or had concretization issues with our base compiler.
 
## Point of Contact

If you need assistance with E4S on Cori please file a ticket at https://help.nersc.gov/. 

If you would like to see your package in E4S, please contact the E4S team:

- Michael A. Heroux (maherou@sandia.gov)

- Sameer Shende (sameer@cs.uoregon.edu)
