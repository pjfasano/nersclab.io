# OpenMP

OpenMP is a specification for a set of compiler directives, library
routines, and environment variables that can be used to specify
high-level parallelism in Fortran and C/C++ programs.
