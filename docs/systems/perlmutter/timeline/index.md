# Perlmutter Timeline

This page records a brief timeline of significant events and user
environment changes on Perlmutter.

The following diagram shows a preliminary, approximate timeline for 
Perlmutter installation and access. Please note that dates are estimates
only, and may change.

![Perlmutter_timeline](../images/perlmutter-timeline.png)

## May 27, 2021

Perlmutter supercomputer dedication.

## November, 2020 - March, 2021
 
Perlmutter Phase 1 delivered.
