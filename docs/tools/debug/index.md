# Debugging Tools

NERSC provides many popular debugging tools. Some of them are
general-purpose tools and others are geared toward more specific
tasks.

A quick guideline on when to use which debugging tool is as follows:

-  [DDT](ddt/index.md): DDT is a parallel debugger that can be run
   with up to 8,192 processors. It has features similar to TotalView
   and and a similarly intuitive user interface. Read more
   [here](ddt/index.md).
-  [GDB](gdb/index.md): GDB can be used to quickly and easily examine
   a core file that was produced when an execution crashed to give
   an approximate traceback. Read more [here](gdb/index.md)
-  [gdb4hpc and CCDB](gdb4hpc_ccdb/index.md): gdb4hpc is a GDB-based
   parallel debugger, developed by Cray. It allows programmers to
   either launch an application or attach to an already-running
   application that was launched with `srun` in order to debug the
   parallel code in command-line mode.  Read more
   [here](gdb4hpc_ccdb/index.md).
-  [Intel Inspector](inspector/index.md): Intel Inspector is a
   memory and threading error-checking tool for users developing
   serial and multithreaded applications on Windows and Linux
   operating systems. Read more [here](inspector/index.md).
-  [Parallelware Analyzer](pwanalyzer/index.md): Parallelware
   Analyzer is a suite of command-line tools aimed at helping
   software developers to build better quality parallel software
   in less time. The tool can detect and fix defects related to
   parallelism with OpenMP and OpenACC. It can also identify
   opportunities for OpenMP/OpenACC parallization on CPUs and GPUs,
   too. Read more [here](pwanalyzer/index.md).
-  [STAT and ATP](stat_atp/index.md): STAT (the Stack Trace Analysis
   Tool) is a highly scalable, lightweight tool that gathers and
   merges stack traces from all of the processes of a parallel
   application. ATP (Abnormal Termination Processing) automatically
   runs STAT when the code crashes. Read more [here](stat_atp/index.md).
-  [TotalView](totalview/index.md): TotalView, from Perforce Software,
   is a parallel debugging tool that can be run with up to 512
   processors. It provides an X Windows-based Graphical User Interface
   and a command line interface. Read more [here](totalview/index.md).
-  [Valgrind](valgrind/index.md): The Valgrind tool suite provides
   several debugging and profiling tools that can help make your
   programs faster and more correct. The most popular tool is
   Memcheck, which can detect many memory-related errors that are
   common in C and C++ programs. Read more [here](valgrind/index.md).
